// (function () {

//   angular
//     .module("UploadApp")
//     .controller("UploadCtrl", UploadCtrl);

//   UploadCtrl.$inject = ["Upload"]; // Upload is a built in service of ngFileUpload

//   function UploadCtrl(Upload) {
//     var vm = this;
//     vm.imgFile = null;
//     vm.status = {
//       message: "",
//       code: 0 
//     }
//     vm.upload = upload;
//     vm.uploadS3 = uploadS3;

//     function upload() {
//       Upload // Upload service from ngFileUpload
//         .upload({
//           url: "/upload",
//           data: {
//             "img-file": vm.imgFile
//           }
//         })
//         .then(function (res) {
//           vm.fileUrl = res.data;
//           vm.status.message = "Upload successful";
//           vm.status.code = res.status;
//           console.log(res.status);
//         })
//         .catch(function (err) {
//           vm.status.message = "Failed";
//           vm.status.code = err.status;
//         })
//     }

//     function uploadS3() {
//       console.log("Upload S3...");
//       Upload // Upload service from ngFileUpload
//         .upload({
//           url: "/uploadS3",
//           data: {
//             "img-file": vm.imgFile
//           }
//         })
//         .then(function (res) {
//           vm.fileUrl = res.data;
//           vm.status.message = "Upload successful";
//           vm.status.code = res.status;
//           console.log(res.status);
//         })
//         .catch(function (err) {
//           vm.status.message = "Failed";
//           vm.status.code = err.status;
//         });
//     }

//   }

// }) ();

(function(){
    angular
        .module("UploadApp")
        .controller("UploadCtrl", UploadCtrl);
    
    // Upload is a built in service of ngFileUpload
    UploadCtrl.$inject = ["Upload"]; 
    
    function UploadCtrl(Upload){
        var vm = this;
        vm.imgFile = null;
        vm.status = {
            message: "",
            code: 0 
        }
        // public functions for the view
        vm.upload = upload;
        vm.uploadS3 = uploadS3;
        
        function upload(){
            console.log("Upload ...");
            Upload.upload({
                url: '/upload',
                data: {
                    "img-file": vm.imgFile,
                }
            }).then(function(response){
                vm.fileUrl = response.data;
                vm.status.message= "Upload successful";
                vm.status.code = response.status;
                console.log(response.status);
            }).catch(function(err){
                vm.status.message= "Failed";
                vm.status.code = err.status;
            });
        }

        function uploadS3(){
            console.log("Upload S3...");
            Upload.upload({
                url: '/uploadS3',
                data: {
                    "img-file": vm.imgFile,
                }
            }).then(function(response){
                vm.fileUrl = response.data;
                vm.status.message= "Upload successful";
                console.log(response.status);
                vm.status.code = response.status;

            }).catch(function(err){
                vm.status.message= "Failed";
                vm.status.code = err.status;
            });
        }

    }
    

})();
